class Board
  attr_accessor :grid, :marks

  def self.blank
    Array.new(3) { Array.new(3) }
  end

  def initialize(grid = Board.blank)
    @grid = grid
    @marks = %i(X O)
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    grid[row][col] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    (grid + columns + diagonals).each do |triple|
      return :X if triple == %i(X X X)
      return :O if triple == %i(O O O)
    end

    nil
  end

  def over?
    grid.flatten.none?(&:nil?) || winner
  end

  def diagonals
    diag1 = [[0, 0], [1, 1], [2, 2]]
    diag2 = [[0, 2], [1, 1], [2, 0]]

    [diag1, diag2].map do |diag|
      diag.map { |row, col| grid[row][col] }
    end
  end

  def columns
    columns = [[], [], []]

    grid.each do |row|
      row.each.with_index do |mark, idx|
        columns[idx] << mark
      end
    end
    columns
  end

end
